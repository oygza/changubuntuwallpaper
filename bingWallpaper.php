<?php
$bingUrl = "https://cn.bing.com";
$url = 'https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&nc=%s&pid=hp';
$time = time()."000";
$wallpaperPath = '/home/oygza/Pictures/bingWallpaper/';
$ext = [];
$res = file_get_contents(sprintf($url,$time));
$res = is_string($res) ? json_decode($res,true) : $res;

if (isset($res['images'][0]['url'])) {
    preg_match('/\w+$/',$res['images'][0]['url'],$ext);
    if (!$ext) {
        echo "parse ext error";
        die;
    }
    $wallpaperPath .= date('Ymd').'.'.$ext[0];
    $imageSteam = file_get_contents($bingUrl.$res['images'][0]['url']);
    file_put_contents($wallpaperPath,$imageSteam);
}
