#!/bin/bash

wallpaperPath="/home/oygza/Pictures/bingWallpaper/"
cd $wallpaperPath
t=$(date +%Y%m%d)
wallpaperName=$wallpaperPath$t".jpg"
if [ ! -f "$t.jpg" ];then
    php -f /data/dev/changubuntuwallpaper/bingWallpaper.php
fi
gsettings set org.gnome.desktop.background picture-uri "$wallpaperName"